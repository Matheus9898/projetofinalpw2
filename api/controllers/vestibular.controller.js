const Vestibular = require('../models/vestibular.model')

module.exports.getAll = async function(req, res){

    let limite = parseInt(req.query.limite)
    let pular = parseInt(req.query.pular) || 0

    if(!req.query.limite){
        res.statusCode = 500
        throw new Error("A consulta precisa ter limite")
    }

    let resultado = await Vestibular.find().limit(limite).skip(pular)
    res.json(resultado)
}

module.exports.getOne = async function(req, res){

    try {
        const resultado = await Vestibular.findById(req.params.id)
        res.json(resultado)

    } catch (error) {
        res.statusCode = 404
        throw new Error("Vestibular não encontrado")
    }
}

module.exports.save = async function(req, res){

    const vestibular = new Vestibular(req.body)
    await vestibular.save()
    res.json(vestibular)
}

module.exports.update = async function(req, res){

    try {
        let id = req.params.id
        let body = req.body
          
        let resultado = await Vestibular.findByIdAndUpdate(id, body)
                
        res.json(resultado)
        
    } catch (error) {
        res.statusCode = 404
        throw new Error("Vestibular não encontrado")   
    }
}

module.exports.delete = async function(req, res){

    try {
        let id = req.params.id
        let resultado = await Vestibular.findByIdAndDelete(id)
        res.json(resultado)
        
    } catch (error) {
        res.statusCode = 404
        throw new Error("Vestibular não encontrado")
    }   
}