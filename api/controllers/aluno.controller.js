const Aluno = require('../models/aluno.model')

module.exports.getAll = async function(req, res){

    let limite = parseInt(req.query.limite)
    let pular = parseInt(req.query.pular) || 0

    if(!req.query.limite){
        res.statusCode = 500
        throw new Error("A consulta precisa ter limite")
    }

    let resultado = await Aluno.find().limit(limite).skip(pular)
    res.json(resultado)
}

module.exports.getOne = async function(req, res){

    try {
        const resultado = await Aluno.findById(req.params.id)
        res.json(resultado)

    } catch (error) {
        res.statusCode = 404
        throw new Error("Aluno não encontrado")
    }
}

module.exports.save = async function(req, res){

    const aluno = new Aluno(req.body)
    await aluno.save()
    res.json(aluno)
}

module.exports.update = async function(req, res){

    try {
        let id = req.params.id
        let body = req.body
          
        let resultado = await Aluno.findByIdAndUpdate(id, body)
                
        res.json(resultado)
        
    } catch (error) {
        res.statusCode = 404
        throw new Error("Aluno não encontrado")   
    }
}

module.exports.delete = async function(req, res){

    try {
        let id = req.params.id
        let resultado = await Aluno.findByIdAndDelete(id)
        res.json(resultado)
        
    } catch (error) {
        res.statusCode = 404
        throw new Error("Aluno não encontrado")
    }   
}