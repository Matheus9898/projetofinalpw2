const mongoose = require('mongoose')
const URL = process.env.MONGOOSE_URL

const connection = mongoose.connect(URL).then(function(){
    console.log("Conexão com banco realizada com sucesso!")
})

module.exports = connection