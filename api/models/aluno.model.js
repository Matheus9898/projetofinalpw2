const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    nome: String,
    nascimento: Number,
    endereco: String,    
    telefone: Number
})

const Aluno = mongoose.model('Aluno', schema)
module.exports = Aluno