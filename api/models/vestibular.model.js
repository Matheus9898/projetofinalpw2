const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    nome: String,
    curso: String,
    nota: Number,
    faculdade: String,
})

const Vestibular = mongoose.model('Vestibular', schema)
module.exports = Vestibular