const express = require('express')
const router = express.Router()
const routes = {
    "vestibular": require('./vestibular.route'),
    "auth": require('./auth.route'),
    "aluno": require('./aluno.route')
}

router.use('/vestibulares', routes.vestibular)
router.use('/alunos', routes.aluno)
router.use('/auth', routes.auth)

module.exports = router
