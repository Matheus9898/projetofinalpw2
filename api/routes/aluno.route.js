const express = require('express')
const router = express.Router()
const controller = require('../controllers/aluno.controller')
const auth = require('../middlewares/authHandler')

router.get('/', auth, controller.getAll)
router.get('/buscar/:id', auth, controller.getOne)
router.post('/salvar', auth, controller.save)
router.put('/atualizar/:id', auth, controller.update)
router.delete('/deletar/:id', auth, controller.delete)

module.exports = router