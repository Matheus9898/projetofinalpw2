Esse projeto implementa uma API de Vestibular/Alunos com Node.js utilizando o framework Express.js e o pacote Mongoose para conexão com banco MongoDB.


Operações:

GET / - retorna lista 
GET /:id - retorna dados com o id especificado
POST / - salva um novo dados com o conteúdo do body (corpo) da requisição
PUT /:id - modifica um dados com o id especificado com o conteúdo do body (corpo) da requisição
DELETE /:id - deleta um dados com o id especificado

Endpoints Alunos:
http://localhost:3000/api/alunos/salvar
http://localhost:3000/api/alunos/buscar/
http://localhost:3000/api/alunos/atualizar/
http://localhost:3000/api/alunos/deletar/
http://localhost:3000/api/alunos?limite=#&pular=#

Endpoints Vestibular:
http://localhost:3000/api/vestibulares/salvar
http://localhost:3000/api/vestibulares//buscar/
http://localhost:3000/api/vestibulares/atualizar/
http://localhost:3000/api/vestibulares/deletar/
http://localhost:3000/api/vestibulares?limite=#&pular=#

Endpoint inicial:
http://localhost:3000/

Endpoint Autenticação:   
http://localhost:3000/api/auth

Link GitLab:
https://gitlab.com/Matheus9898/projetofinalpw2

Link Heroku:
https://projetofinalpw2.herokuapp.com/
